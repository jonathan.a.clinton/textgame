package textGame;

public class GameStart {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//this is where the program starts running since this is where we have our main method
		//must initialize world
		//must load avatars
		
		//when it ends, all data must be saved (persistence)
		
		UserInterface ui = new UserInterface();
		ui.launch();
		
	}

}
