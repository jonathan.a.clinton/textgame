package textGame;

public class Avatar {

	
	//we will probably want fields to identify the avatar and give information about it health, level, etc
	private String name;
	private int charID;
	private int health;
	private int level;
	//etc
	
	//we declare these fields private so that only classes in our textGame will be able to see those fields
	//if we didnt declare them private then anybody could access them and screw them up
	//of course outside classes still need a way of figuring out the data in these fields and so the class will have"setter" and "getter methods
	//these methods allow for safer access and it is good style in java to keep your fields private and supply these methods
	//ex:
	public String getName(){
		return new String(name);//this creates a copy of name and returns that copy that way our original data can't be edited
	}
	public void setName(String n){
		name = n;//if we wanted we could add more protection than this
	}
	//we would want to add all of these methods to our Avatar class and then we would most likely follow that up with more "active" methods
	//so we might have a move function - which means we should probably have a private location field
	//we would probably want to have some roll functionality if we wanted this to be like d&d that will give us a value based on their stats and java "randomness"
}
