package textGame;

import java.util.Scanner;

public class UserInterface {

		//we will have to give good input to the user to explain how the game works/introduction
		//we want to focus on making the commands that they have at their disposal as simple as possible
			//so we should have as few commands as possible to accomplish what we want with simple arguments to those commands
			//arguments are the values that are passed into the function
		//we will need to provide a help method that they can call any time they are confused
	
	//this launch method will be what is called by GameStart after loading everything else so that the user can start playing
	public void launch(){
		//the Scanner is how we will take input from the user
		Scanner in = new Scanner(System.in);
		//we will store the user input that we retrieve from the Scanner object in this input string
		String input;
		System.out.println("The game is on.");
		
		//usually we would have to know whether the game has been launched before as to whether they are continuing a game or starting from scratch
		System.out.println("Enter a character name.");
		//we now store what the user enters in our input string
		input = in.nextLine();
		
		//this is how we create a new Avatar "object"
		Avatar firstAvatar = new Avatar();
		//everything in java is an object
		//World is an object, we can call methods on that object or use methods that are in that object
		//our UserInterface is an object and our GameStart main method will call our UserInterface's launch method that we have here
		
		//now that we have our avatar created, we should probably give it the name that the user asked us to give it
		//firstAvatar is just an arbitrary name that I chose for this instance of the Avatar object - an instance is a particular case of an object
		//the way we call an objects method is we have an instance of that object(firstAvatar) 
				//using that object we use . to access either one of that avatar's public fields, or public methods
				//in this case we are accessing the public method setName which takes in a string parameter
				//so we pass in our input string that we got from the user up above
		firstAvatar.setName(input);
		
		//we now have set the firstAvatar's name to the input that the user gave us
		//if we wanted we could have them create a second avatar - 
		System.out.println("Enter a companion name.");
		input = in.nextLine();//this will write over the previous input but that is alright since we have already stored it in firstAvatar's name field
		Avatar secondAvatar = new Avatar();//here is our second avatar
		secondAvatar.setName(input);
		
		//now we should probably show the user what they set the names too just to verify
		//this line is quite complicated
		//in java you can do string concatenation with + where you put half of the string before and half after
		//our Avatar.getName() function is also called
		//this getName function returns a string as output though
		//so we are doing a string literal ("you chose " ) concatenated with the string returned from firstAvatar.getName() concatenated with
				//another string literal ("as your character name")
		System.out.println("You chose " + firstAvatar.getName() + " as your character name");
		//then we could do the same with our secondAvatar
		

		in.close();//since we are done using our Scanner object for now, we should close it so that we don't waste system resources- dont worry about this for now
	}
	
}
